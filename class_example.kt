open class Animal {
    open val image = ""
    open val food = ""
    open val habitat = ""
    var hunger = 10
    
    open fun makeNoise() {
        println("The Animal is making a noise")
    }
    
    open fun eat() {
        println("The Animal is eating")
    }
    
    open fun roam() {
        println("The Animal is roaming")
    }
    
    open fun sleep() {
        println("The Animal is sleeping")
    }
}

class Wolf : Animal(){
    override val image = "bigbadwolf.png"
    override val food = "Hippo"
    override val habitat = "Savannah"
    override fun makeNoise() {
    	println("The Wolf is howling")
    }
    
    override fun eat() {
        println("The Wolf is eating the ${this.food}")
    }
}

class Hippo : Animal(){
    override val image = "hippopotatomus.png"
    override val habitat = "Savannah"
    override val food = "grass"
    override fun makeNoise() {
    	println("The Hippo is grunting")
    }
    
    override fun eat() {
        println("The Hippo is eating some ${this.food}")
    }
}

fun main(){
    val bigBadOne = Wolf()
    val savannahWaterCow = Hippo()
    //
    savannahWaterCow.eat()
    savannahWaterCow.makeNoise()
    bigBadOne.makeNoise()
    bigBadOne.eat()
    savannahWaterCow.makeNoise()
    bigBadOne.eat()
    bigBadOne.eat()
}








